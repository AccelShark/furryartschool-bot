import discord

class Client(discord.Client):
    def __init__(self, token):
        super().__init__()
        self.run(token)

    async def on_ready(self):
        print('Logged on as {0}!'.format(self.user))

    async def on_message(self, message):
        print('Message from {0.author}: {0.content}'.format(message))