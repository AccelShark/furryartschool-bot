from setuptools import setup, find_packages

setup(
    # This is FurryArtBot
    name='furryartbot',
    # Version Number...
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
)