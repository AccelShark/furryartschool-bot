import dialog
import toml
from typing import IO

# Magic numbers
auth_version = 1

d = dialog.Dialog("dialog")
d.set_background_title("FurryArtBot")

def open_toml(f: IO):
    auth = {}
    auth_okay = False
    if "version" in f:
        auth_okay = f["version"] >= auth_version and f["okay"] == "true"

    if not auth_okay:
        prompt = d.yesno("Are you sure you want to overwrite your access token locally? This is only useful if you are a new bot admin.")
        auth["version"] = auth_version
        auth_okay = prompt == d.OK

        if auth_okay:
            ok, entry = d.passwordbox("Please enter in your super secret FurryArtBot API token:")
            auth["okay"] = ok == d.OK
            auth["token"] = entry
            toml.dump(auth, f)

try:
    with open("Authfile.toml", "w+") as f:
        open_toml(f)
except FileNotFoundError:
    with open("Authfile.toml", "x") as f:
        open_toml(f)
